Code Style
===
This project follows PEP 8 and uses
[black](https://github.com/python/black) to format the code
automatically according to it.


Requirements
===
To work on this project, you'll need:
1. python 3.5 or higher
2. pipenv
3. mariadb server running
4. npm
5. (optionally) [black](https://github.com/python/black) for
   automatically formatting code.

Setting up development environment
===
1. Fork and clone the repository:
   `git clone URL`
2. Use pipenv to create virtualenv and install dependencies: 
   `cd wikigrade; pipenv install --dev`
3. Build the static assets:
   `(cd evaluate/assets; npm install && npx gulp)`
4. Create a mysql user for wikigrade and grant it privileges to create
   the main and test DB:
   ```sql
       CREATE USER wikigrade IDENTIFIED BY 'long random password';
       GRANT ALL PRIVILEGES ON wikigrade.* TO wikigrade;
       GRANT ALL PRIVILEGES ON wikigrade_test.* TO wikigrade;
   ```
5. Create the database:
   ```sql
       CREATE DATABASE wikigrade
         DEFAULT CHARACTER SET = 'utf8mb4'
         DEFAULT COLLATE = 'utf8mb4_general_ci';
   ```
6. Create a `.env` file by coping the template:
   `cp .env.sample .env`
7. Edit `.env` file and fill in the database credentials
8. Launch a shell inside the virtualenv:
   `pipenv shell`
9. Make sure all tests run fine:
   `./manage.py test`
   (To speed up subsequent runs, you can use `./manage.py test --keepdb`)
10. Run migrations:
    `./manage.py migrate`
11. Start wikigrade:
    `./manage.py runserver 8080`
