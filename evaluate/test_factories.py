from datetime import date, datetime
from pytz import timezone

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

import factory
from factory.django import DjangoModelFactory as ModelFactory

from .models.core import Article, ArticleScore, Contest, Contribution


def _dummy_calculate(self):
    """Avoid NotImplementedError from ArticleScore on save()."""
    return self.value if self.value else -1


ArticleScore.calculate = _dummy_calculate  # type: ignore


class ContestFactory(ModelFactory):
    class Meta:
        model = Contest

    title = "Test contest"
    wiki = "uk"
    start_date = date(2001, 1, 1)
    end_date = date(2001, 2, 1)
    categories = ""
    reviewer_usernames = ""
    timezone = "Europe/Kiev"
    scoring_type = factory.LazyFunction(
        lambda: ContentType.objects.get_for_model(ArticleScore)
    )


class ContestReadyFactory(ContestFactory):
    last_import_datetime = datetime(2001, 2, 2, tzinfo=timezone("UTC"))


class UserFactory(ModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda n: "User {}".format(n))

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        return cls._get_manager(model_class).create_user(*args, **kwargs)


class ArticleFactory(ModelFactory):
    class Meta:
        model = Article

    pageid = factory.Sequence(lambda n: n)
    title = factory.Sequence(lambda n: "Article {}".format(n))


class ContribFactory(ModelFactory):
    class Meta:
        model = Contribution

    delta = 3500
    num_edits = 1
    latest_revision = 5
    disqualified = ""
    username = factory.Sequence(lambda n: "ContribUser {}".format(n))
    article = factory.SubFactory(ArticleFactory)


class ScoreFactory(ModelFactory):
    class Meta:
        model = ArticleScore

    reviewer = factory.SubFactory(UserFactory)


class EvaluateTest(TestCase):
    """A base class for model-related testing.

    Creates and saves a contest, an article and a contribution.
    """

    def setUp(self):
        self.contest = ContestFactory()
        self.article = ArticleFactory()
        self.contest.articles.add(self.article)
        self.reviewer = UserFactory()
        self.contribution = ContribFactory(
            article=self.article, contest=self.contest
        )
