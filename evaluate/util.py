from typing import Any, Dict, List, NamedTuple, Sequence, Type
from urllib.parse import urljoin

from django.contrib.auth.mixins import UserPassesTestMixin
from django.db.models import Model
from django.utils.translation import gettext_lazy as _


class MustBeReviewerMixin(UserPassesTestMixin):
    """A view mixin making sure the user is a reviewer for the contest.

    Requires a get_contest() method to be defined on the view,
    returning the current contest.
    """

    raise_exception = True
    permission_denied_message = _(
        "Must be authenticated as a reviewer to evaluate articles."
    )

    def test_func(self):
        if not self.request.user.is_authenticated:
            return False
        if self.request.user.is_superuser:
            return True
        contest = self.get_contest()
        return contest.username_is_reviewer(self.request.user.username)


FieldDisplay = NamedTuple(
    "FieldDisplay", [("name", str), ("verbose_name", str), ("value", Any)]
)


def model_to_fields_display(
    instance: Type[Model], exclude: Sequence = None
) -> List[FieldDisplay]:
    """Convert a model instance to a list of fields.

    Each element in the list is an instance of FieldDisplay. This
    allows easy iteration over model's fields in a template without
    the knowledge of the specific fields.

    For fields that have choices set, the value is set to what
    `get_FIELD_display()` returns.
    """
    result = []
    exclude = exclude or ()
    for field in instance._meta.get_fields():
        if field.name in exclude:
            continue
        try:
            display_getter = getattr(
                instance, "get_{}_display".format(field.name)
            )
            value = display_getter()
        except AttributeError:
            value = getattr(instance, field.name)
        verbose_name = getattr(field, "verbose_name", None)
        result.append(FieldDisplay(field.name, verbose_name, value))
    return result


def wiki_url(wiki: str, append: str = "") -> str:
    """Return a wiki URL for a given Wikipedia language."""
    return urljoin("https://{}.wikipedia.org/".format(wiki), append)
