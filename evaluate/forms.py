import pytz
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from typing import Type
from .models.core import ArticleScore, Contest, Contribution


def _fields_order(field: str) -> int:
    """A sort key function that puts the comment field last."""
    if field == "comment":
        return 1
    else:
        return 0


def get_scoring_form(scoring_model: Type[ArticleScore]) -> forms.ModelForm:
    """Return a ModelForm for a specified ArtisleScore-based model."""
    fields_sorted = sorted(
        [f.name for f in scoring_model._meta.fields], key=_fields_order
    )

    class ScoringForm(forms.ModelForm):
        class Meta:
            model = scoring_model
            exclude = ("article", "reviewer", "contribution", "value")
            fields = fields_sorted
            widgets = {"comment": forms.Textarea(attrs={"rows": 3})}

    return ScoringForm


class DisqualifyForm(forms.ModelForm):
    class Meta:
        model = Contribution
        fields = ("disqualified", "disqualified_comment")


class ContributionSeenForm(forms.Form):
    seen = forms.BooleanField(required=False)

    def clean_seen(self) -> bool:
        """Make the form only consider the field as False when the field is
        present in the request (might be empty), but raise exception
        when the field is missing."""
        if "seen" not in self.data:
            raise ValidationError(
                "Missing field: %(field)s",
                code="missing_field",
                params={"field": "seen"},
            )
        else:
            return self.cleaned_data["seen"]


class ContestModelForm(forms.ModelForm):
    class Meta:
        model = Contest
        exclude = []

    def clean_timezone(self):
        try:
            tz = pytz.timezone(self.cleaned_data["timezone"])
        except pytz.UnknownTimeZoneError:
            raise ValidationError(_("Unknown timezone"), code="timezone")
        return self.cleaned_data["timezone"]
