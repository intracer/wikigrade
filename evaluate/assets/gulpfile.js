const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const webpack = require('webpack-stream');

gulp.task('sass', function () {
  var sassOptions = {
    includePaths: 'node_modules/foundation-sites/scss/'
  };
  return gulp.src('./scss/**/*.scss')
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer({
            // Foundation's recommended settings
            browsers: ['last 2 versions', 'ie >= 9', 'android >= 4.4', 'ios >= 7']
        }))
        .pipe(gulp.dest('../static/evaluate/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./scss/**/*.scss', ['sass']);
});

gulp.task('js', function () {
    return gulp.src('./js/evaluation.js')
        .pipe(webpack({
            output: {filename: 'bundle.js'},
        }))
        .pipe(gulp.dest('../static/evaluate/js/'))
});

gulp.task('js:watch', function () {
    gulp.watch('./js/*.js', ['js']);
});

gulp.task('default', ['sass', 'js']);
gulp.task('watch',  ['default', 'sass:watch', 'js:watch'])
