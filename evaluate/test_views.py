from datetime import datetime
from django.urls import reverse
from django.test import Client, TestCase

from .models.core import Contest

from .test_factories import (
    ContestFactory,
    ContestReadyFactory,
    ContribFactory,
    ScoreFactory,
    UserFactory,
)


class TestContestList(TestCase):
    url = reverse("contest_list")

    def setUp(self):
        self.c = Client()
        dates = (
            datetime(2010, 1, 1, 0, 0, 0),
            datetime(2009, 1, 1, 0, 0, 0),
            datetime(2012, 1, 1, 0, 0, 0),
        )
        Contest.objects.bulk_create(
            [ContestFactory.build(start_date=d) for d in dates]
        )

    def test_contest_list_order(self):
        """Ensure the view returns the contests in start date ascending
        order."""
        contests = Contest.objects.order_by("start_date")
        response = self.c.get(self.url)
        self.assertListEqual(list(contests), list(response.context["contests"]))


class TestContestEvaluateTabs(TestCase):
    """Test Contribution list tabs and filters."""

    def setUp(self):
        self.contest = ContestReadyFactory()
        self.contribs = {
            None: ContribFactory(contest=self.contest),
            "evaluated": ContribFactory(contest=self.contest),
            "disqualified": ContribFactory(
                contest=self.contest, disqualified="other"
            ),
        }
        self.score = ScoreFactory(contribution=self.contribs["evaluated"])
        self.c = Client()

    def request_tab_contribs(self, tab=None):
        """Perform a GET request to the specific tab of the view and return a
        list of contributions the view returned."""
        url = reverse("contest_evaluate", args=(self.contest.pk,))
        if tab:
            url = "{}?tab={}".format(url, tab)
        response = self.c.get(url)
        return list(response.context["contribs"])

    def test_tabs(self):
        for tab in (None, "evaluated", "disqualified"):
            with self.subTest(tab=tab):
                self.assertEqual(
                    self.request_tab_contribs(tab), [self.contribs[tab]]
                )


class TestContestEvaluatePerUser(TestCase):
    """Test Contribution list "only mine" vs "everyone's" filter."""

    def setUp(self):
        self.reviewer = UserFactory()
        self.contest = ContestReadyFactory(
            reviewer_usernames=self.reviewer.username
        )
        self.contrib_for_reviewer = ContribFactory(
            contest=self.contest, assigned_to=self.reviewer.username
        )
        self.contrib_unassigned = ContribFactory(contest=self.contest)
        self.contrib_for_others = ContribFactory(
            contest=self.contest, assigned_to="Random"
        )
        self.c = Client()
        self.c.force_login(self.reviewer)
        self.url = reverse("contest_evaluate", args=(self.contest.pk,))

    def test_defaults_to_only_mine(self):
        c = Client()  # No cookies
        c.force_login(self.reviewer)
        response = c.get(self.url)
        self.assertTrue(response.context["only_mine"])

    def test_all_contribs(self):
        response = self.c.get(self.url + "?only_mine=0")
        self.assertListEqual(
            list(response.context["contribs"]),
            list(self.contest.contributions.all()),
        )

    def test_mine_contribs(self):
        response = self.c.get(self.url + "?only_mine=1")
        self.assertTrue(response.context["only_mine"])
        self.assertSetEqual(
            set(response.context["contribs"]), set([self.contrib_for_reviewer])
        )


class TestContestSupervise(TestCase):
    def setUp(self):
        self.reviewer = UserFactory()
        self.contest = ContestFactory(reviewer_usernames=self.reviewer.username)
        self.contribs = {
            "unseen": [ContribFactory(contest=self.contest)],
            "seen": [
                ContribFactory(seen_by=self.reviewer, contest=self.contest),
                ContribFactory(seen_by=self.reviewer, contest=self.contest),
            ],
        }
        self.c = Client()
        self.c.force_login(self.reviewer)
        self.url = reverse(
            "contest_supervise", kwargs={"contest_id": self.contest.pk}
        )

    def test_supervise_unseen_default(self):
        response = self.c.get(self.url)
        self.assertSetEqual(
            set(response.context["contribs"]), set(self.contribs["unseen"])
        )

    def test_supervise_seen(self):
        response = self.c.get(self.url + "?tab=seen")
        self.assertSetEqual(
            set(response.context["contribs"]), set(self.contribs["seen"])
        )


class TestContributionSeen(TestCase):
    def setUp(self):
        self.reviewer = UserFactory()
        self.contest = ContestFactory(reviewer_usernames=self.reviewer.username)
        self.c = Client()
        self.c.force_login(self.reviewer)

    @staticmethod
    def contrib_url(contrib):
        return reverse("contrib_seen", kwargs={"pk": contrib.pk})

    def test_contrib_set_seen(self):
        contrib = ContribFactory(contest=self.contest)
        self.c.post(self.contrib_url(contrib), {"seen": 1})
        contrib.refresh_from_db()
        self.assertEqual(contrib.seen_by, self.reviewer)

    def test_contrib_set_unseen(self):
        contrib = ContribFactory(contest=self.contest, seen_by=self.reviewer)
        self.c.post(self.contrib_url(contrib), {"seen": ""})
        contrib.refresh_from_db()
        self.assertEqual(contrib.seen_by, None)

    def test_response_redirect_url(self):
        contrib = ContribFactory(contest=self.contest)
        response = self.c.post(self.contrib_url(contrib), {"seen": 1})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response["Location"],
            reverse(
                "contest_supervise", kwargs={"contest_id": self.contest.pk}
            ),
        )

    def test_rejects_missing_field(self):
        contrib = ContribFactory(contest=self.contest)
        response = self.c.post(self.contrib_url(contrib), {"irrelevant": 1})
        self.assertEqual(response.status_code, 400)


class TestEvaluation(TestCase):
    def setUp(self):
        self.reviewer = UserFactory(username="Trustworthy")
        self.contest = ContestReadyFactory(
            reviewer_usernames=self.reviewer.username
        )
        self.contrib = ContribFactory(contest=self.contest, continuous=False)
        self.c = Client()
        self.c.force_login(self.reviewer)
        self.url = reverse("article_evaluation", kwargs={"pk": self.contrib.pk})

    def test_evaluate_redirect_correct(self):
        response = self.c.post(self.url, {"submit-evaluate": "notempty"})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response["Location"],
            reverse(
                "contest_evaluate",
                kwargs={"contest_id": self.contrib.contest.pk},
            ),
        )

    def test_evaluate_create_score(self):
        self.c.post(
            self.url,
            {"submit-evaluate": "notempty", "comment": "A valid comment"},
        )
        score = self.contrib.scores.first()
        self.assertEqual(score.comment, "A valid comment")

    def test_disqualify(self):
        self.c.post(
            self.url,
            {
                "submit-disqualify": "notempty",
                "disqualified": "other",
                "disqualified_comment": "A valid reason",
            },
        )
        self.contrib.refresh_from_db()
        self.assertEqual(self.contrib.disqualified, "other")
        self.assertEqual(self.contrib.disqualified_by, self.reviewer)
        self.assertEqual(self.contrib.disqualified_comment, "A valid reason")

    def test_undisqualify(self):
        self.contrib.disqualified = "other"
        self.contrib.disqualified_by = self.reviewer
        self.contrib.disqualified_comment = "A valid reason"
        self.contrib.save()

        self.c.post(self.url, {"submit-undisqualify": "notempty"})
        self.contrib.refresh_from_db()
        self.assertEqual(self.contrib.disqualified, "")
        self.assertEqual(self.contrib.disqualified_by, None)
        self.assertEqual(
            self.contrib.disqualified_comment, "Undisqualified by Trustworthy."
        )

    def test_warns_non_continuous(self):
        response = self.c.get(self.url)
        self.assertEqual(len(response.context["warnings"]), 1)


class TestEvaluationNotReady(TestCase):
    """Ensure evaluation views refuse access if contest is not ready for
    evaluation (see `Contest.ready_for_evaluation`)."""

    def setUp(self):
        self.reviewer = UserFactory()
        self.contest = ContestFactory(reviewer_usernames=self.reviewer.username)
        self.contrib = ContribFactory(contest=self.contest)
        self.c = Client()
        self.c.force_login(self.reviewer)

    def assert_400(self, url):
        """Ensure URL returns a 400 response code."""
        response = self.c.get(url)
        self.assertEqual(response.status_code, 400)

    def test_contribution_evaluate_not_ready(self):
        url = reverse("article_evaluation", kwargs={"pk": self.contrib.pk})
        self.assert_400(url)

    def test_contest_evaluate_not_ready(self):
        url = reverse(
            "contest_evaluate", kwargs={"contest_id": self.contest.pk}
        )
        self.assert_400(url)

    def test_contest_progress_not_ready(self):
        url = reverse("contest_progress", kwargs={"pk": self.contest.pk})
        self.assert_400(url)

    def test_contest_result_not_ready(self):
        url = reverse("contest_result", kwargs={"pk": self.contest.pk})
        self.assert_400(url)
