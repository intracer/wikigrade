import importlib
import uuid
from datetime import datetime

import pytz
import celery

from unittest.mock import MagicMock, patch
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.shortcuts import render
from django.test import TestCase, RequestFactory, Client, override_settings
from django.urls import reverse

from .test_factories import (
    ArticleFactory,
    ContestFactory,
    ContestReadyFactory,
    ContribFactory,
    UserFactory,
    ScoreFactory,
    EvaluateTest,
)
from .forms import ContestModelForm
from .network import query_articles, query_revisions
from .models.core import Article, ArticleScore, Contribution, get_scoring_types
from .views import model_to_fields_display, UserContribs
from .templatetags.wiki_links import comment_on_wiki_link
from .services import import_articles

import evaluate.tasks as tasks


class QueryArticles(TestCase):
    def setUp(self):
        self.contest = ContestFactory()

    def _response_categorymembers(self, **kwargs):
        """A mock for MediaWiki.request() yielding category members that are
        pages in the main namespace."""
        responses = [
            {
                "query": {
                    "pages": [
                        {
                            "contentmodel": "wikitext",
                            "lastrevid": 21889899,
                            "length": 3143,
                            "ns": 0,
                            "pageid": 721999,
                            "pagelanguage": "uk",
                            "pagelanguagedir": "ltr",
                            "pagelanguagehtmlcode": "uk",
                            "title": "Відкритий вміст",
                            "touched": "2018-01-31T15:35:02Z",
                        }
                    ]
                }
            },
            {
                "query": {
                    "pages": [
                        {
                            "contentmodel": "wikitext",
                            "lastrevid": 19442163,
                            "length": 6220,
                            "ns": 0,
                            "pageid": 339012,
                            "pagelanguage": "uk",
                            "pagelanguagedir": "ltr",
                            "pagelanguagehtmlcode": "uk",
                            "title": "Зоряні мапи (програма)",
                            "touched": "2018-05-17T07:38:36Z",
                        }
                    ]
                }
            },
        ]
        for portion in responses:
            yield portion

    def _response_categorymembers_talkpages(self, **kwargs):
        """A mock for MediaWiki.request() yielding category members that are
        talk pages with subjectid set to the id of the ns0-page."""
        responses = [
            {
                "query": {
                    "pages": [
                        {
                            "contentmodel": "wikitext",
                            "lastrevid": 22313001,
                            "length": 111,
                            "new": True,
                            "ns": 1,
                            "pageid": 2686417,
                            "pagelanguage": "uk",
                            "pagelanguagedir": "ltr",
                            "pagelanguagehtmlcode": "uk",
                            "subjectid": 2686415,
                            "title": "Обговорення:1001 грам",
                            "touched": "2018-05-12T14:28:11Z",
                        }
                    ]
                }
            },
            {
                "query": {
                    "pages": [
                        {
                            "contentmodel": "wikitext",
                            "lastrevid": 22335108,
                            "length": 51,
                            "new": True,
                            "ns": 1,
                            "pageid": 2689008,
                            "pagelanguage": "uk",
                            "pagelanguagedir": "ltr",
                            "pagelanguagehtmlcode": "uk",
                            "subjectid": 2689006,
                            "title": "Обговорення:Абіску (дослідницька станція)",
                            "touched": "2018-05-12T14:28:11Z",
                        }
                    ]
                }
            },
        ]
        for portion in responses:
            yield portion

    def test_query_articles_main(self):
        with patch(
            "evaluate.network.MediaWiki.request",
            self._response_categorymembers_talkpages,
        ):
            articles = list(query_articles(self.contest))
            self.assertEqual(
                articles,
                [
                    (2686415, "1001 грам"),
                    (2689006, "Абіску (дослідницька станція)"),
                ],
            )

    def test_query_articles_talkpages(self):
        with patch(
            "evaluate.network.MediaWiki.request", self._response_categorymembers
        ):
            articles = list(query_articles(self.contest))
            self.assertEqual(
                articles,
                [
                    (721999, "Відкритий вміст"),
                    (339012, "Зоряні мапи (програма)"),
                ],
            )


class TestContributionSize(TestCase):
    """Make sure the Contribution size is calculated correctly."""

    def setUp(self):
        self.contest = ContestFactory()
        self.article = ArticleFactory()

    def test_calculate_revision_delta(self):
        """Test revision deltas are calucated correctly."""
        revisions = [{"size": 4}, {"size": 5}, {"size": 3}]
        Contribution._revisions_add_deltas(revisions, 2)
        self.assertEqual(
            revisions,
            [
                {"size": 4, "size_delta": 2},
                {"size": 5, "size_delta": 1},
                {"size": 3, "size_delta": -2},
            ],
        )

    def test_accumulate_bytes(self):
        """Test multiple revision deltas are accumulated correctly."""
        revisions = [
            {"size_delta": 55},
            {"size_delta": -15},
            {"size_delta": 20},
        ]
        result = Contribution._accumulate_bytes(revisions)
        self.assertEqual(result, 60)

    def test_contribution_bytes_different_users(self):
        """Test Contribution size with interleaved edits of multiple
        people with negative deltas."""
        revisions = (
            {"size_delta": 4, "user": "A", "revid": 0},
            {"size_delta": -2, "user": "B", "revid": 1, "parentid": 0},
            {"size_delta": 4, "user": "A", "revid": 2, "parentid": 1},
        )
        deltas = {"A": 8, "B": -2}
        _deltas = {
            c.username: c.delta
            for c in Contribution.objects.create_from_revisions(
                contest=self.contest, article=self.article, revisions=revisions
            )
        }
        self.assertEqual(deltas, _deltas)

    def test_contribution_bytes_same_user(self):
        """Test a Contribution size with single user edits with negative
        deltas."""
        revisions = (
            {"size_delta": 4, "user": "A", "revid": 0},
            {"size_delta": -2, "user": "A", "revid": 1, "parentid": 0},
            {"size_delta": 4, "user": "A", "revid": 2, "parentid": 1},
        )
        deltas = {"A": 6}
        _deltas = {
            c.username: c.delta
            for c in Contribution.objects.create_from_revisions(
                contest=self.contest, article=self.article, revisions=revisions
            )
        }
        self.assertEqual(deltas, _deltas)


class TestContributionContinuous(TestCase):
    def test_continuous(self):
        """Test a continous contribution."""
        revisions = (
            {"size_delta": 2, "user": "A", "revid": 0},
            {"size_delta": 2, "user": "A", "revid": 1, "parentid": 0},
            {"size_delta": 2, "user": "A", "revid": 2, "parentid": 1},
        )
        self.assertTrue(Contribution._check_is_continuous(revisions))

    def test_non_continuous(self):
        """Test a continous contribution."""
        revisions = (
            {"size_delta": 2, "user": "A", "revid": 0},
            {"size_delta": 2, "user": "A", "revid": 1, "parentid": 0},
            {"size_delta": 2, "user": "A", "revid": 4, "parentid": 3},
        )
        self.assertFalse(Contribution._check_is_continuous(revisions))


class TestModelDictDisplay(EvaluateTest):
    def test_with_choices(self):
        self.contribution.disqualified = "other"
        display = model_to_fields_display(self.contribution)
        field = next(filter(lambda f: f.name == "disqualified", display))
        self.assertEquals(field.value, "Other reason")

    def test_without_choices(self):
        display = model_to_fields_display(self.contribution)
        field = next(filter(lambda f: f.name == "delta", display))
        self.assertEquals(field.value, 3500)

    def test_all_fields(self):
        display = model_to_fields_display(self.contribution)
        should_contain = set(
            (
                "id",
                "article",
                "contest",
                "username",
                "delta",
                "num_edits",
                "latest_revision",
                "disqualified",
                "continuous",
                "disqualified_by",
                "disqualified_comment",
                "assigned_to",
                "scores",
                "seen_by",
            )
        )
        self.assertEquals(set([f.name for f in display]), should_contain)

    def test_exclude_fields(self):
        display = model_to_fields_display(
            self.contribution, exclude=("id", "scores")
        )
        should_contain = set(
            (
                "article",
                "contest",
                "username",
                "delta",
                "num_edits",
                "latest_revision",
                "disqualified",
                "continuous",
                "disqualified_by",
                "disqualified_comment",
                "assigned_to",
                "seen_by",
            )
        )
        self.assertEquals(set([f.name for f in display]), should_contain)


class TestContestResult(TestCase):
    def setUp(self):
        self.contest = ContestFactory()
        self.reviewers = [UserFactory() for i in range(0, 2)]

        # Generate a bunch of articles
        c = self.contest
        Article.objects.bulk_create(
            [ArticleFactory.build() for i in range(1, 5)]
        )
        c.articles.add(*Article.objects.all())

        # Contributions for the Winner
        Contribution.objects.bulk_create(
            [
                ContribFactory.build(article=a, contest=c, username="Winner")
                for a in c.articles.all()[:2]
            ]
        )
        # Contributions for the Non-winner
        Contribution.objects.bulk_create(
            [
                ContribFactory.build(
                    article=a, contest=c, username="Non-winner"
                )
                for a in c.articles.all()[2:]
            ]
        )

        # Scores
        winner_contribs = c.contributions.filter(username="Winner")
        ArticleScore.objects.bulk_create(
            [
                ArticleScore(
                    contribution=winner_contribs[0],
                    reviewer=self.reviewers[0],
                    value=100,
                ),
                ArticleScore(
                    contribution=winner_contribs[0],
                    reviewer=self.reviewers[1],
                    value=50,
                ),
                ArticleScore(
                    contribution=winner_contribs[1],
                    reviewer=self.reviewers[0],
                    value=50,
                ),
            ]
        )
        ArticleScore.objects.bulk_create(
            [
                ArticleScore(
                    contribution=contrib, reviewer=self.reviewers[0], value=50
                )
                for contrib in c.contributions.filter(username="Non-winner")
            ]
        )

        # Create a bunch of unevaluated contributions
        unevaluated_article = ArticleFactory()
        Contribution.objects.bulk_create(
            [
                ContribFactory.build(
                    article=unevaluated_article,
                    contest=c,
                    username="Random user %d" % i,
                )
                for i in range(0, 15)
            ]
        )

    def test_contributor_scores(self):
        """Test calculating contributor scores in a contest.

        This includes averaging contributions with more than one score.
        """
        expected = [
            {"username": "Winner", "sum": 125.0, "contribs": 2},
            {"username": "Non-winner", "sum": 100.0, "contribs": 2},
        ]
        result = ArticleScore.get_result_contributors(self.contest)
        self.assertEquals(expected, list(result))

    def test_disqualified(self):
        """Test disqualified contributions are not counted."""
        contrib = self.contest.contributions.filter(username="Winner").first()
        contrib.disqualified = "coi"
        contrib.save()

        expected = [
            {"username": "Non-winner", "sum": 100.0, "contribs": 2},
            {"username": "Winner", "sum": 50.0, "contribs": 1},
        ]
        result = ArticleScore.get_result_contributors(self.contest)
        self.assertEquals(expected, list(result))


class ContestReviewers(EvaluateTest):
    def test_reviewer_list_empty(self):
        """Test empty list is returned on empty reviewer list."""
        self.assertEqual(list(self.contest.get_reviewers()), [])

    def test_reviewer_list_whitespace_only(self):
        """Test empty list is returned on whitespace-only reviewer list."""
        self.contest.reviewer_usernames = "  \n  "
        self.assertEqual(list(self.contest.get_reviewers()), [])

    def test_reviewer_list_includes_whitespace_lines(self):
        """Test no empty strings are included for when
        reviewer_usernames contains empty lines among usernames."""
        self.contest.reviewer_usernames = "User1  \n  \nUser2 \n"
        self.assertEqual(list(self.contest.get_reviewers()), ["User1", "User2"])


class TestPermissions(EvaluateTest):
    def setUp(self):
        super().setUp()
        self.contest.last_import_datetime = datetime(
            2019, 1, 1, tzinfo=pytz.timezone("UTC")
        )
        self.contest.save()
        self.client = Client()

    def assert_anonymous_403(self, url):
        """Try visiting a url and check response code is 403."""
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_evaluation_anonymous(self):
        """Test anonymous users can't evaluate articles."""
        self.assert_anonymous_403(
            reverse("article_evaluation", kwargs={"pk": self.contribution.id})
        )

    def test_score_anonymous(self):
        """Test anonymous users can't see details of a score."""
        score = ScoreFactory(contribution=self.contribution)
        self.assert_anonymous_403(
            reverse("score_detail", kwargs={"pk": score.id})
        )

    def test_contest_result_anonymous(self):
        """Test anonymous users can't view contest results."""
        self.assert_anonymous_403(
            reverse("contest_result", kwargs={"pk": self.contest.id})
        )

    def test_contest_result_raw_anonymous(self):
        """Test anonymous users can't export contest scores."""
        self.assert_anonymous_403(
            reverse("contest_scores_export", kwargs={"pk": self.contest.id})
        )

    def test_supervise_anonymous(self):
        """Test anonymous users can't visit the supervise view."""
        self.assert_anonymous_403(
            reverse("contest_supervise", kwargs={"contest_id": self.contest.id})
        )


class TestUserContribsView(TestCase):
    """Test UserContribs view returns correct results."""

    def setUp(self):
        # Create a few contributions
        self.reviewer = UserFactory()
        self.contest = ContestFactory(reviewer_usernames=self.reviewer.username)

        # Create articles in reverse order to check results are ordered
        articles = [
            ArticleFactory(title="Article {}".format(i))
            for i in range(4, 0, -1)
        ]
        Contribution.objects.bulk_create(
            [
                ContribFactory.build(
                    article=a,
                    contest=self.contest,
                    delta=6000,
                    username="Contributor",
                )
                for a in articles
            ]
        )

    def test_user_contribs_view(self):
        """Test UserContribs view returns all valid contributions by a certain
        contributor."""
        factory = RequestFactory()
        request = factory.get("/irrelevant")
        request.user = self.reviewer
        response = UserContribs.as_view()(
            request, contest_id=self.contest.pk, username="Contributor"
        )
        article_titles = [
            c.article.title for c in response.context_data["contribs"]
        ]
        self.assertEqual(
            article_titles, ["Article 1", "Article 2", "Article 3", "Article 4"]
        )


class TestContribsEvaluated(EvaluateTest):
    """Test a contribuiton is treated like evaluated if it has enough scores."""

    def setUp(self):
        super().setUp()
        scores = [
            ArticleScore(
                contribution=self.contribution, reviewer=UserFactory(), value=50
            )
            for i in range(0, 5)
        ]
        ArticleScore.objects.bulk_create(scores)

    def test_enough_scores(self):
        self.assertEqual(
            ArticleScore.evaluated_contribs_q(
                Contribution.objects.all()
            ).count(),
            1,
        )
        self.assertEqual(
            ArticleScore.evaluated_contribs_q(
                Contribution.objects.all(), evaluated=False
            ).count(),
            0,
        )

    def test_not_enough_scores(self):
        with patch(
            "evaluate.models.core" ".ArticleScore.MIN_SCORES_PER_CONTRIB", 10
        ):
            self.assertEqual(
                ArticleScore.evaluated_contribs_q(
                    Contribution.objects.all()
                ).count(),
                0,
            )
            self.assertEqual(
                ArticleScore.evaluated_contribs_q(
                    Contribution.objects.all(), evaluated=False
                ).count(),
                1,
            )


class TestCommentOnWikiLink(TestCase):
    def setUp(self):
        contest = ContestFactory(wiki="uk")
        article = ArticleFactory(title="Hello World")
        self.contrib = ContribFactory(
            contest=contest, article=article, username="Foo"
        )

    def test_comment_on_wiki_link(self):
        result = comment_on_wiki_link(self.contrib)
        correct = (
            "https://uk.wikipedia.org/wiki/User%20talk%3AFoo"
            "?action=edit&section=new&preloadtitle="
            "%5B%5BHello+World%5D%5D+%28Test+contest%29"
        )
        self.assertEqual(result, correct)


class TestContestReadyForEvaluation(TestCase):
    def setUp(self):
        self.contest = ContestFactory(
            start_date=datetime(2019, 1, 1),
            end_date=datetime(2019, 2, 1),
            timezone="Europe/Kiev",
        )
        self.utc = pytz.timezone("UTC")

    def test_not_ready_for_evaluation(self):
        self.contest.last_import_datetime = datetime(
            2019, 1, 31, tzinfo=self.utc
        )
        self.assertFalse(self.contest.ready_for_evaluation)

    def test_not_ready_for_evaluation_same_second(self):
        self.contest.last_import_datetime = datetime(
            2019, 2, 1, tzinfo=pytz.timezone("Europe/Kiev")
        )
        self.assertFalse(self.contest.ready_for_evaluation)

    def test_ready_for_evaluation_timezone(self):
        # 23 in UTC is the next day in Kyiv
        self.contest.last_import_datetime = datetime(
            2019, 1, 31, 23, tzinfo=self.utc
        )
        self.assertTrue(self.contest.ready_for_evaluation)

    def test_ready_for_evaluation(self):
        self.contest.last_import_datetime = datetime(
            2019, 2, 1, 0, 0, 5, tzinfo=self.utc
        )
        self.assertTrue(self.contest.ready_for_evaluation)

    def test_not_ready_when_not_imported(self):
        self.assertFalse(self.contest.ready_for_evaluation)


class TestContestImportDateTime(TestCase):
    """Ensure import_articles is setting import date only if the import
    was successful.
    """

    NOW = datetime(2002, 1, 1, tzinfo=pytz.utc)  # Set a static now

    def setUp(self):
        self.contest = ContestReadyFactory()

    @override_settings(CELERY_SINGLETON_CLASS=celery.Task)
    def test_import_sets_task_id(self):
        importlib.reload(tasks)  # Needed to apply the above setting
        with patch("evaluate.tasks.import_articles"):
            result = tasks.contest_import_articles.apply(args=[self.contest.pk])
        self.contest.refresh_from_db()
        self.assertEqual(str(self.contest.import_task_id), result.task_id)

    def test_failed_import_doesnt_set_date(self):
        was = self.contest.last_import_datetime
        try:
            with patch("evaluate.services.query_articles") as _qa, patch(
                "evaluate.services.query_revisions"
            ):
                _qa.side_effect = ValueError("Simulating failure!")
                import_articles(self.contest.pk)
        except ValueError:
            pass
        self.assertEqual(self.contest.last_import_datetime, was)

    def test_import_succeeded_date(self):
        with patch("evaluate.services.query_articles") as _qa, patch(
            "evaluate.services.query_revisions"
        ) as _qv, patch("evaluate.services.datetime") as _datetime:
            _qa.return_value = [(1, "Hello")]
            _qv.return_value = ([], 0)
            _datetime.now.return_value = self.NOW
            import_articles(self.contest)
        self.assertEqual(self.contest.last_import_datetime, self.NOW)


class TestSafeImport(TestCase):
    """Ensure some operations can not be performed while contest import is
    running."""

    def setUp(self):
        self.contest = ContestFactory(import_task_id=uuid.uuid4())
        self.c = Client()
        self.superuser = User.objects.create_superuser(
            "admin", "admin@example.com", "password"
        )
        self.c.force_login(self.superuser)
        self.url = reverse("admin:contest_services", args=(self.contest.pk,))
        self.actions = [
            "_import_service",
            "_assign_service",
            "_disqualify_coi_service",
        ]

    @staticmethod
    def task_status_mock(status: str) -> MagicMock:
        async_result = MagicMock()
        task = MagicMock()
        task.state = status
        async_result.return_value = task
        return async_result

    def test_operations_blocked_during_import(self):
        with patch(
            "evaluate.tasks.AsyncResult", self.task_status_mock("PENDING")
        ):
            for action in self.actions:
                with self.subTest(action=action):
                    response = self.c.post(self.url, {action: ""})
                    self.assertEqual(response.status_code, 400)

    def test_operations_allowed_after_import(self):
        http200_mock = MagicMock()
        http200_mock.return_value = HttpResponse("all fine")
        with patch(
            "evaluate.tasks.AsyncResult", self.task_status_mock("SUCCESS")
        ), patch(
            "evaluate.admin.ContestAdmin.import_service", http200_mock
        ), patch(
            "evaluate.admin.ContestAdmin.assign_service", http200_mock
        ), patch(
            "evaluate.admin.ContestAdmin.disqualify_coi_service", http200_mock
        ):
            for action in self.actions:
                with self.subTest(action=action):
                    response = self.c.post(self.url, {action: ""})
                    self.assertEqual(response.status_code, 200)


class ContestValidTimezone(TestCase):
    def test_contest_modelform_timezone_rejected(self):
        form = ContestModelForm()
        form.cleaned_data = {"timezone": "Invalid/Invalid"}
        with self.assertRaises(ValidationError):
            form.clean_timezone()

    def test_admin_rejects_invalid_timezone(self):
        url = reverse("admin:evaluate_contest_add")
        c = Client()
        superuser = User.objects.create_superuser(
            "admin", "admin@example.com", "password"
        )
        c.force_login(superuser)

        response = c.post(
            url,
            {
                "timezone": "Invalid/Invalid",
                "title": "",
                "start_date": "2019-01-01",
                "end_date": "2019-12-31",
                "scoring_type": (
                    ContentType.objects.filter(get_scoring_types()).first().pk
                ),
            },
        )
        self.assertIn("timezone", response.context["adminform"].errors)
