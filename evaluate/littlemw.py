"""A very light-weight wrapper for MediaWiki API.

Has the following specific goals:

- automate requests that require continuation;
- cache responses during development.
"""

import requests

from django.conf import settings

if settings.DEBUG:
    import requests_cache


class MediaWikiError(RuntimeError):
    pass


class MediaWiki:
    API_DEFAULTS = {
        "action": "query",
        "format": "json",
        "formatversion": 2,
        "maxlag": 5,
    }

    def __init__(
        self, url: str, user_agent: str = settings.MEDIAWIKI_USER_AGENT
    ) -> None:
        """
        Args:
            url: An API endpoint url to send requests to.
        """
        self.url = url
        if settings.DEBUG:
            # Cache all external requests indefinitely,
            # regardless of any http cache headers
            self.session = requests_cache.core.CachedSession("mediawiki-cache")
        else:
            self.session = requests.Session()
        if user_agent:
            self.session.headers.update({"User-Agent": user_agent})

    def request(self, **kwargs):
        """Perform a request to the API endpoint.

        Args:
            Keyword arguments that will be sent to the API
                as GET parameters.

        Yields:
            The results decoded from json. On each iteration the next
            portion of the results is fetched using MediaWiki
            continuation.

        Raises:
            MediaWikiError: If MediaWiki returned an explicit error.
        """
        params = self.API_DEFAULTS.copy()
        params.update(kwargs)

        if params["format"] != "json":
            raise ValueError("Only JSON format is supported for MediaWiki API.")

        while True:
            response = self._do_request(params).json()

            if "error" in response:
                raise MediaWikiError(
                    "MediaWiki signalled an error: {}."
                    "Original query params: {}".format(
                        response["error"], params
                    )
                )

            yield response

            if "continue" in response:
                params.update(response["continue"])
            else:
                break

    def _do_request(self, params):
        """Perform the actual request to the API."""
        return self.session.get(self.url, params=params)
