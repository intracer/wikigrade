"""The "business logic" of the application, i.e. the top high-level
operations of the app.
"""
from datetime import datetime
import pytz
import logging

from .models.core import Contest, Article, Contribution
from .network import query_articles, query_revisions

from typing import cast


logger = logging.getLogger(__name__)


def import_articles(contest: Contest) -> int:
    """Import articles participating in contest from Mediawiki, analyze
    article history and save contributions. If an article aleady exists in
    the database, it and its contributions are updated.

    Returns:
      The number of articles in the contest (pre-existing and new).
    """
    # Capture the time when import started
    import_started = datetime.now(tz=pytz.utc)

    # Articles themselves
    for pageid, title in query_articles(contest):
        article, created = Article.objects.update_or_create(
            pageid=pageid, wiki=contest.wiki, defaults={"title": title}
        )
        article.contest.add(contest)
        if created:
            logger.debug('Added new article "{}"'.format(article))
        else:
            logger.debug('Updated article "{}"'.format(article))

    # Revisions and contributions
    for article in contest.articles.iterator():
        revisions, before_size = query_revisions(article, contest)
        if revisions:
            Contribution._revisions_add_deltas(
                revisions,
                # mypy workaround -
                # it's always an int
                cast(int, before_size),
            )
            contribs = Contribution.objects.create_from_revisions(
                contest=contest, article=article, revisions=revisions
            )
            if contribs:
                logger.debug(
                    'Created {} contributions for article "{}".'.format(
                        len(contribs), article
                    )
                )
        else:
            logger.debug(
                'Article "{}" has no contributions during contest.'.format(
                    article
                )
            )

    contest.last_import_datetime = import_started
    contest.save()
    return contest.articles.count()
