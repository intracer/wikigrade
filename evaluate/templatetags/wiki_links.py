from urllib.parse import urlencode, quote

from django import template

from ..models.core import Contribution
from ..util import wiki_url

register = template.Library()


@register.filter(name="comment_on_wiki_link")
def comment_on_wiki_link(contribution: Contribution) -> str:
    section_title = "[[{article}]] ({contest})".format(
        article=contribution.article, contest=contribution.contest
    )
    target = "/wiki/User talk:" + contribution.username
    params = (
        ("action", "edit"),
        ("section", "new"),
        ("preloadtitle", section_title),
    )
    return wiki_url(
        contribution.contest.wiki, quote(target) + "?" + urlencode(params)
    )
