from unittest.mock import patch
from django.test import TestCase

from .models import ArticleScore
from .models.scores import (
    WikiLovesMusicScore,
    CEESpring2018Score,
    WikiKharkiv2018Score,
)
from .test_factories import (
    ArticleFactory,
    ContestFactory,
    ContribFactory,
    ScoreFactory,
    EvaluateTest,
)

from typing import Optional, Type, Mapping, Any


class WikiLovesMusicScoreFactory(ScoreFactory):
    class Meta:
        model = WikiLovesMusicScore

    relevance = 10
    coverage = 10
    appearence = 10
    contrib_type = WikiLovesMusicScore.CONTRIB_TYPES.new.value
    contributed_wikidata = False


class WikiKharkiv2018ScoreFactory(ScoreFactory):
    class Meta:
        model = WikiKharkiv2018Score

    quality = 3
    is_old = False


class ScoreTest(EvaluateTest):
    """A base class for testing scores.

    In addition to contest, article and a contributoion, creates an
    unsaved score instance under self.score.

    Class attributes:
        SCORING_MODEL (model): A class of a scoring model to test.
        SCORING_DEFAULTS (dict): An optional fields and their values
            to set on the score object.

    """

    SCORING_MODEL = None  # type: Optional[Type[ArticleScore]]
    SCORING_DEFAULTS = {}  # type: Mapping[str, Any]

    def setUp(self):
        super().setUp()
        if not self.SCORING_MODEL:
            raise RuntimeError(
                "Please set SCORING_MODEL attribute on the class"
            )
        params = self.SCORING_DEFAULTS.copy()
        params.update(
            {"reviewer": self.reviewer, "contribution": self.contribution}
        )
        self.score = self.SCORING_MODEL(**params)


class TestWikiLovesMusicScore(ScoreTest):
    SCORING_MODEL = WikiLovesMusicScore
    SCORING_DEFAULTS = {"relevance": 10, "coverage": 10, "appearence": 10}

    def test_size_coefficient(self):
        size_value = {
            3500: 0.5,
            10000: 0.7793660969657374,
            15000: 0.887263617950123,
            150000: 1.5,
        }
        for size, coef in size_value.items():
            with self.subTest(size=size, coef=coef):
                self.assertAlmostEquals(self.score.size_coefficient(size), coef)

    def test_score_base_values(self):
        s = self.score
        s.contrib_type = s.CONTRIB_TYPES.new.value

        self.assertEqual(s.calculate(), 12.5)

    def test_score_delta_value(self):
        s = self.score
        s.contribution.delta = 150000
        self.assertEqual(s.calculate(), 25 * 1.5)

    def test_score_contrib_type_value(self):
        s = self.score

        s.contrib_type = s.CONTRIB_TYPES.improved.value
        self.assertEqual(s.calculate(), 12.5 * 0.95)

        s.contrib_type = s.CONTRIB_TYPES.translation.value
        self.assertEqual(s.calculate(), 12.5 * 0.9)

        s.contrib_type = s.CONTRIB_TYPES.list_translation.value
        self.assertEqual(s.calculate(), 12.5 * 0.5)

        s.contrib_type = s.CONTRIB_TYPES.new_list.value
        self.assertEqual(s.calculate(), 12.5 * 0.95)

    def test_score_wikidata_value(self):
        s = self.score
        s.contributed_wikidata = True
        self.assertEqual(s.calculate(), 12.5 * 1.1)


class TestCEESpring2018Score(ScoreTest):
    SCORING_MODEL = CEESpring2018Score

    def test_size_coefficient(self):
        s = self.score
        for case in (
            ({"delta": 3500, "is_list": False}, 0.8),
            ({"delta": 5000, "is_list": False}, 1),
            ({"delta": 7000, "is_list": False}, 1),
            ({"delta": 10000, "is_list": False}, 1.1),
            ({"delta": 10000, "is_list": True}, 0.8),
        ):
            with self.subTest(repr(case[0])):
                self.assertEqual(s.size_coefficient(**case[0]), case[1])

    def test_score_nonlist(self):
        s = self.score
        s.contribution.delta = 3800
        s.quality = 1
        s.is_list = False
        s.is_obligatory = False

        # (3800 / 1000.0) * 1 * 0.8 * 1
        self.assertAlmostEqual(s.calculate(), 3.04)

    def test_score_list_nonoriginal(self):
        s = self.score
        s.contribution.delta = 15000
        s.quality = 0.5
        s.is_list = True
        s.is_obligatory = False

        # (15000 / 1000.0) * 0.5 * 0.8 * 1
        self.assertAlmostEqual(s.calculate(), 6.0)

    def test_score_obligatory(self):
        s = self.score
        s.contribution.delta = 6000
        s.quality = 2
        s.is_list = False
        s.is_obligatory = True

        # (6000 / 1000) * 2 * 1 * 2
        self.assertAlmostEqual(s.calculate(), 24)

    def test_score_size(self):
        s = self.score
        s.quality = 1
        s.is_list = False
        s.is_obligatory = False

        for case in (
            # (4999 / 1000.0) * 1 * 0.8 * 1
            (4999, 3.9992),
            # (5000 / 1000.0) * 1 * 1 * 1
            (5000, 5.0),
            # (10000 / 1000.0) * 1 * 1.1 * 1
            (10000, 11.0),
        ):
            with self.subTest("Size: {}".format(case[0])):
                s.contribution.delta = case[0]
                self.assertAlmostEqual(s.calculate(), case[1])


class TestWikiLovesMusicResult(TestCase):
    def setUp(self):
        self.contest = ContestFactory()
        self.articles = [ArticleFactory() for i in range(0, 10)]
        self.contest.contributions.bulk_create(
            [
                ContribFactory.build(
                    contest=self.contest, username="ContribUser", article=a
                )
                for a in self.articles
            ]
        )

    def test_new_articles_lt_10_excluded(self):
        """New articles with score < 10 must not be included in contest result."""
        # These two must be counted
        s = WikiLovesMusicScoreFactory(
            contrib_type=WikiLovesMusicScore.CONTRIB_TYPES.new.value,
            contribution=self.contest.contributions.all()[0],
        )
        s.articlescore_ptr.value = 50
        s.articlescore_ptr.save()

        s = WikiLovesMusicScoreFactory(
            contrib_type=WikiLovesMusicScore.CONTRIB_TYPES.improved.value,
            contribution=self.contest.contributions.all()[1],
        )
        s.articlescore_ptr.value = 5
        s.articlescore_ptr.save()

        # This one mustn't
        s = WikiLovesMusicScoreFactory(
            contrib_type=WikiLovesMusicScore.CONTRIB_TYPES.new.value,
            contribution=self.contest.contributions.last(),
        )
        s.articlescore_ptr.value = 4.99
        s.articlescore_ptr.save()

        expected = [{"username": "ContribUser", "sum": 55, "contribs": 2}]
        result = WikiLovesMusicScore.get_result_contributors(self.contest)
        self.assertEquals(expected, list(result))


class TestWikiKharkiv2018Score(TestCase):
    def setUp(self):
        self.contest = ContestFactory()
        self.article = ArticleFactory()
        self.contest.articles.add(self.article)
        self.contrib = ContribFactory(
            article=self.article, contest=self.contest, delta=4000
        )
        self.score = WikiKharkiv2018ScoreFactory(contribution=self.contrib)

    def test_size(self):
        """Test the contribution size is correctly considered in the formula."""
        for param in [
            (4500, 13.5),  # 3 * 4500 / 1000 * 1
            (3501, 10.503),  # 3 * 3501 / 1000 * 1
            (12000, 36.0),
        ]:  # 3 * 12000 / 1000 * 1
            with self.subTest(delta=param[0], value=param[1]):
                self.contrib.delta = param[0]
                self.assertEqual(self.score.calculate(), param[1])

    def test_old(self):
        """Test the 'improved article' modifier."""
        with self.subTest(is_old=True):
            self.score.is_old = True
            # 3 * 4000 / 1000 * 0.95
            self.assertAlmostEqual(self.score.calculate(), 11.39999999)

        with self.subTest(is_old=False):
            # 3 * 4000 / 1000 * 1
            self.score.is_old = False
            self.assertEqual(self.score.calculate(), 12)

    def test_quality(self):
        """Test the quality is correctly considered."""
        for param in [
            (6.3, 25.2),  # 6.3 * 4000 / 1000 * 1
            (2.1, 8.4),
        ]:  # 2.1 * 4000 / 1000 * 1
            with self.subTest(quality=param[0], value=param[1]):
                self.score.quality = param[0]
                self.assertEqual(self.score.calculate(), param[1])
